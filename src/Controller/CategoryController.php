<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Form\CategoryType;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/category')]
class CategoryController extends AbstractController
{
    #[Route('/new', name: 'app_category_new')]
    #[IsGranted('ROLE_ADMIN')]
    public function new(Request $request,ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->renderForm('category/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/edit/{id}', name: 'app_category_edit')]
    #[IsGranted('ROLE_ADMIN')]
    public function edit(int $id, Request $request,ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $categoryRepository = $doctrine->getRepository(Category::class);
        $category = $categoryRepository->find($id);

        $form = $this->createForm(CategoryType::class, $category);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->renderForm('category/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'app_category_delete')]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(int $id,ManagerRegistry $doctrine,Request $request): Response
    {

        $categoryRepository = $doctrine->getRepository(Category::class);
        $category = $categoryRepository->find($id);

        $entityManager = $doctrine->getManager();
        $boards = $category->getBoards();
        foreach ($boards as $board) {
            $topics = $board->getTopics();
            foreach ($topics as $topic) {
                $replies = $topic->getReplies();
                foreach($replies as $reply){
                    $entityManager->remove($reply);
                }
                $entityManager->remove($topic);
            }
            $entityManager->remove($board);
        }
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('app_home');

    }

    #[Route('/{id}', name: 'app_category')]
    public function index(int $id, ManagerRegistry $doctrine): Response
    {

        $categoryRepository = $doctrine->getRepository(Category::class);
        $category = $categoryRepository->find($id);

        foreach ($category->getRoles() as $role) {
            $this->denyAccessUnlessGranted($role);
        }

        $boards = $category->getBoards();

        return $this->render('category/index.html.twig', [
            'category' => $category,
            'boards' => $boards
        ]);
    }
}
