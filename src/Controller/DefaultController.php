<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Category;
use App\Entity\Board;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(ManagerRegistry $doctrine): Response
    {

        $categoryRepository = $doctrine->getRepository(Category::class);
        $categories = $categoryRepository->findAll();

        $boardRepository = $doctrine->getRepository(Board::class);
        $boards = $boardRepository->findAll();

        return $this->render('default/index.html.twig', [
            'categories' => $categories,
            'boards' => $boards
        ]);
    }
}
