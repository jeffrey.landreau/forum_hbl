<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Board;
use App\Entity\Category;
use App\Form\BoardType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/board')]
class BoardController extends AbstractController
{
    

    #[Route('/{category_id}/new', name: 'app_board_new')]
    #[IsGranted('ROLE_USER')]
    public function new(int $category_id , Request $request,ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();
        $board = new Board();
        $categoryRepository = $doctrine->getRepository(Category::class);

        foreach ($categoryRepository->find($category_id)->getRoles() as $role) {
            $this->denyAccessUnlessGranted($role);
        }

        $form = $this->createForm(BoardType::class, $board);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $board = $form->getData();
            $board->setCategory($categoryRepository->find($category_id));
            $entityManager->persist($board);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->renderForm('board/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'app_board_delete')]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(int $id,ManagerRegistry $doctrine,Request $request): Response
    {

        $boardRepository = $doctrine->getRepository(Board::class);
        $board = $boardRepository->find($id);
        $cid = $board->getCategory()->getId();

        $entityManager = $doctrine->getManager();
        $topics = $board->getTopics();
            foreach ($topics as $topic) {
                $replies = $topic->getReplies();
                foreach($replies as $reply){
                    $entityManager->remove($reply);
                }
                $entityManager->remove($topic);
            }
        $entityManager->remove($board);
        $entityManager->flush();

        return $this->redirectToRoute('app_category',['id' => $cid]);

    }

    #[Route('/{id}', name: 'app_board')]
    public function index(int $id, ManagerRegistry $doctrine): Response
    {

        $boardRepository = $doctrine->getRepository(Board::class);
        $board = $boardRepository->find($id);

        foreach ($board->getCategory()->getRoles() as $role) {
            $this->denyAccessUnlessGranted($role);
        }

        $topics = $board->getTopics();

        return $this->render('board/index.html.twig', [
            'topics' => $topics,
            'board' => $board
        ]);
    }
}
