<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Topic;
use App\Form\TopicType;
use App\Entity\Board;
use App\Entity\Reply;
use App\Form\ReplyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

#[Route('/topic')]
class TopicController extends AbstractController
{

    

    #[Route('/{board_id}/new', name: 'app_topic_new')]
    #[IsGranted('ROLE_USER')]
    public function new(int $board_id,Request $request,ManagerRegistry $doctrine ): Response
    {
        $entityManager = $doctrine->getManager();
        $topic = new Topic();
        $boardRepository = $doctrine->getRepository(Board::class);

        foreach ($boardRepository->find($board_id)->getCategory()->getRoles() as $role) {
            $this->denyAccessUnlessGranted($role);
        }

        $form = $this->createForm(TopicType::class, $topic);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $topic = $form->getData();
            $topic->setUser($this->getUser());
            $topic->setBoard($boardRepository->find($board_id));
            $topic->setDateCreation(new \DateTime());
            $entityManager->persist($topic);
            $entityManager->flush();

            return $this->redirectToRoute('app_home');
        }

        return $this->renderForm('topic/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{id}/delete', name: 'app_topic_delete')]
    #[IsGranted('ROLE_ADMIN')]
    public function delete(int $id,ManagerRegistry $doctrine,Request $request): Response
    {

        $topicRepository = $doctrine->getRepository(Topic::class);
        $topic = $topicRepository->find($id);
        $bid = $topic->getBoard()->getId();

        $replies = $topic->getReplies();

        $entityManager = $doctrine->getManager();
        
        foreach ($replies as $reply) {
            $entityManager->remove($reply);
        }
        $entityManager->remove($topic);
        $entityManager->flush();

        return $this->redirectToRoute('app_board',['id' => $bid]);

    }

    #[Route('/reply/{id}/delete', name: 'app_reply_delete')]
    #[IsGranted('ROLE_ADMIN')]
    public function deleteReply(int $id,ManagerRegistry $doctrine,Request $request): Response
    {

        $replyRepository = $doctrine->getRepository(Reply::class);
        $reply = $replyRepository->find($id);
        $tid = $reply->getTopic()->getId();

        $entityManager = $doctrine->getManager();
        
        $entityManager->remove($reply);
        $entityManager->flush();

        return $this->redirectToRoute('app_topic',['id' => $tid]);

    }

    #[Route('/{id}', name: 'app_topic')]
    public function index(int $id,ManagerRegistry $doctrine,Request $request): Response
    {

        $topicRepository = $doctrine->getRepository(Topic::class);
        $topic = $topicRepository->find($id);

        foreach ($topic->getBoard()->getCategory()->getRoles() as $role) {
            $this->denyAccessUnlessGranted($role);
        }

        $replies = $topic->getReplies();

        $entityManager = $doctrine->getManager();
        $reply = new Reply();
        $form = $this->createForm(ReplyType::class, $reply);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reply = $form->getData();
            $reply->setUser($this->getUser());
            $reply->setTopic($topic);
            $reply->setDateCreation(new \DateTime());
            $entityManager->persist($reply);
            $entityManager->flush();

            return $this->redirectToRoute('app_topic',['id' => $id]);
        }

        return $this->renderForm('topic/index.html.twig', [
            'form' => $form,
            'topic' => $topic,
            'replies' => $replies
        ]);
    }

    


}
