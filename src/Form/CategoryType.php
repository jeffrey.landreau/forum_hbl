<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('roles', ChoiceType::class, array(
                'choices' => 
                array
                (
                    'ROLE_COLLABORATOR' => array
                    (
                        'Collaborator' => 'ROLE_COLLABORATOR',
                    ),
                    'ROLE_INSIDER' => array
                    (
                        'Insider' => 'ROLE_INSIDER'
                    ),
                    'ROLE_EXTERNAL' => array
                    (
                        'External' => 'ROLE_EXTERNAL'
                    ),
                ) 
                ,
                'expanded' => true,
                'multiple' => true,
                'required' => true,
                ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
